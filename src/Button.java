
public class Button {
  private final String text;
  private final float x, y, width, height;
  
  public Button(String text, float x, float y, float width, float height) {
    this.text = text;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  public String getText() {
    return text;
  }

  public float getX() {
    return x;
  }
  
  public float getY() {
    return y;
  }
  
  public float getWidth() {
    return width;
  }

  public float getHeight() {
    return height;
  }
}
