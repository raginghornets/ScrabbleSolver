public class Tile {
  private final float size, offsetX, offsetY, spacing;
  private float initX, initY, x, y;
  private char value;
  private boolean selected;
  private byte id;
  
  public Tile(char val) {
    x = 0;
    y = 0;
    initX = 0;
    initY = 0;
    size = 50;
    offsetX = size;
    offsetY = size/5;
    spacing = 12.2f;
    value = val;
    id = 0;
  }
  
  public String toString() {
    return Character.toString(value);
  }
  
  public float getInitX() {
    return initX;
  }

  public float getInitY() {
    return initY;
  }
  
  public char getValue() {
    return value;
  }

  public void setValue(char value) {
    this.value = value;
  }
  
  public float getX() {
    return x;
  }

  public float getY() {
    return y;
  }
  
  public float getSize() {
    return size;
  }
  
  public float getOffsetX() {
    return offsetX;
  }
  
  public float getOffsetY() {
    return offsetY;
  }
  
  public float getSpacing() {
    return spacing;
  }
  
  public void setPosition(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  public void setInitPosition(float x, float y) {
    this.initX = x;
    this.initY = y;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }
  
  public byte getId() {
    return id;
  }

  public void setId(byte id) {
    this.id = id;
  }
  
  public void empty() {
    value = '?';
  }

  public boolean isEmpty() {
    return this.value == '?';
  }
  
  public boolean isBlank() {
    return this.value == '*';
  }
  
  public int score() {
    switch(value) {
      case 'A': return 1;
      case 'B': return 3;
      case 'C': return 3;
      case 'D': return 2;
      case 'E': return 1;
      case 'F': return 4;
      case 'G': return 2;
      case 'H': return 4;
      case 'I': return 1;
      case 'J': return 8;
      case 'K': return 5;
      case 'L': return 1;
      case 'M': return 3;
      case 'N': return 1;
      case 'O': return 1;
      case 'P': return 3;
      case 'Q': return 10;
      case 'R': return 1;
      case 'S': return 1;
      case 'T': return 1;
      case 'U': return 1;
      case 'V': return 4;
      case 'W': return 4;
      case 'X': return 8;
      case 'Y': return 4;
      case 'Z': return 10;
      default: return 0;
    }
  }
  
}
