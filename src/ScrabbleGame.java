import java.util.ArrayList;

public class ScrabbleGame {
  
  private final Tile[][] board;
  private final short WIDTH;
  private final short HEIGHT;
  private final Button resetButton;
  private final Button drawButton;
  private final ArrayList<Player> playersList;
  private final ScrabbleSolver scrabbleSolver;
  private final ArrayList<Tile> tilesRemaining;
  private final byte maxLengthCurrentTiles;
  
  private Tile selectedTile;
  private Player currentPlayer;
  private String bestWord;
  private ArrayList<String> possibleWords;
  private boolean endGame;
  private boolean displayingScreenSolver;
  
  public ScrabbleGame(byte numPlayers) {
    board = new Tile[15][15];
    WIDTH = 1000;
    HEIGHT = 1000;
    resetButton = new Button("Reset", 17, HEIGHT - 68.5f, 140, 50);
    drawButton = new Button("Draw", WIDTH - 150, HEIGHT - 68.5f, 132.5f, 50);
    playersList = new ArrayList<Player>();
    scrabbleSolver = new ScrabbleSolver();
    tilesRemaining = new ArrayList<Tile>();
    selectedTile = new Tile('?');
    endGame = false;
    displayingScreenSolver = false;
    maxLengthCurrentTiles = 7;
    wordsRemoveAboveMax();
    addPlayers(numPlayers);
    addTilesToTilesRemaining();
    emptyBoard();
    allPlayersDraw();
    determineStartingPlayer();
    updateScrabbleSolver();
  }
  
  public String toString() {
    return board();
  }
  
  public String board() {
    String res = "";
    for(int r = 0; r < board.length; r++) {
      for(int c = 0; c < board[r].length; c++) {
        res += " " + board[c][r] + " ";
        if(c >= board[r].length - 1) {
          res += "\n";
        }
      }
    }
    return res;
  }
  
  /**
   * <ol>
   * <li> Set board tile id to selected tile id </li>
   * <li> Set board tile value to selected tile value </li>
   * <li> Add score to current player </li>
   * <li> Empty selected tile </li>
   * <li> Return tile </li>
   * </ol>
   */
  public void place(int r, int c) {
    if(!selectedTile.isEmpty() && !selectedTile.isBlank() && validTilePlacement(r, c)) {
      board[r][c].setId(selectedTile.getId());
      board[r][c].setValue(selectedTile.getValue());
      currentPlayer.addScore(board[r][c].score());
      selectedTile.empty();
      returnTile();
    }
  }
  
  /**
   * Pick up board tile
   * <ol>
   * <li> Undo tile score </li>
   * <li> Set next available current tile's value to board tile's value </li>
   * <li> Empty board tile </li>
   * <li> Update the scrabble solver </li>
   * </ol>
   */
  public void pickUpTile(int r, int c) {
    if(!board[r][c].isEmpty() && selectedTile.isEmpty()) {
      for(Tile tile : currentPlayer.getCurrentTiles()) {
        if(tile.isEmpty() && board[r][c].getId() == currentPlayer.getId()) {
          currentPlayer.undoScore(board[r][c].score());
          tile.setValue(board[r][c].getValue());
          board[r][c].empty();
          updateScrabbleSolver();
          select(tile);
          break;
        }
      }
    }
  }
  
  /**
   * Return selected tile back to current tiles
   * <ol>
   * <li> Reset selected tile </li>
   * <li> Deselect all current tiles </li>
   * <li> Reset current tile position </li>
   * </ol>
   */
  public void returnTile() {
    for(Tile tile : currentPlayer.getCurrentTiles()) {
      if(tile.isSelected()) {
        selectedTile = new Tile('?');
        currentPlayer.deselectTiles();
        tile.setPosition(tile.getInitX(), tile.getInitY());
        break;
      }
    }
  }
  
  /**
   * <ol>
   * <li> Return tile </li>
   * <li> Assign currentPlayer to next player in playerList</li>
   * <li> Update the scrabble solver </li>
   * </ol>
   */
  public void nextTurn() {
    returnTile();
    if(playersList.indexOf(currentPlayer) < playersList.size() - 1) {
      currentPlayer = playersList.get(playersList.indexOf(currentPlayer) + 1);
    } else {
      currentPlayer = playersList.get(0);
    }
    updateScrabbleSolver();
  }
  
  /**
   * <ol>
   * <li> Return tile </li>
   * <li> Assign currentPlayer to previous player in playerList</li>
   * <li> Update the scrabble solver </li>
   * </ol>
   */
  public void previousTurn() {
    returnTile();
    if(playersList.indexOf(currentPlayer) > 0) {
      currentPlayer = playersList.get(playersList.indexOf(currentPlayer) - 1);
    } else {
      currentPlayer = playersList.get(playersList.size() - 1);
    }
    updateScrabbleSolver();
  }
  
  /**
   * <ol>
   * <li> Set tile to selected </li>
   * <li> Set tile id to current player id </li>
   * <li> Selected tile is assigned to tile </li>
   * </ol>
   */
  public void select(Tile t) {
    if(selectedTile.isEmpty() && !t.isEmpty()) {
      t.setSelected(true);
      t.setId(currentPlayer.getId());
      selectedTile = t;
    }
  }
  
  /**
   * <ol>
   * <li> Assign bestWord to worth with most points from the current tiles </li>
   * <li> Assign possibleWords to the possible words from the current tiles </li>
   * </ol>
   */
  public void updateScrabbleSolver() {
    bestWord = scrabbleSolver.wordWorthMostPoints(scrabbleSolver.possibleWords(currentPlayer.currentTilesToString()));
    possibleWords = scrabbleSolver.possibleWords(currentPlayer.currentTilesToString());
  }

  /**
   * @return Player with highest score
   */
  public Player winningPlayer() {
    Player winningPlayer = playersList.get(0);
    for(Player player : playersList) {
      if(player.getScore() > winningPlayer.getScore()) {
        winningPlayer = player;
      }
    }
    return winningPlayer;
  }
  
  /**
   * @return True if every player is out of tiles
   */
  public boolean noMoreTurns() {
    for(Player player : playersList) {
      for(Tile tile : player.getCurrentTiles()) {
        if(!tile.isEmpty()) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * <ul>
   * <li> Determine who will be starting player and assign current player to that player </li>
   * <li> The starting player is the one who has the highest alphabetical ranking for their first tile </li>
   * </ul>
   */
  private void determineStartingPlayer() {
    Player startingPlayer = playersList.get(0);
    for(Player player : playersList) {
      char playerValue = Character.toLowerCase(player.getCurrentTiles().get(0).getValue());
      char firstPlayerValue = Character.toLowerCase(startingPlayer.getCurrentTiles().get(0).getValue());
      if(playerValue > firstPlayerValue) {
        startingPlayer = player;
      }
    }
    currentPlayer = startingPlayer;
  }
  
  private void allPlayersDraw() {
    for(Player player : playersList) {
      player.drawTiles(tilesRemaining);
    }
  }
  
  /**
   * Add n players to playersList
   */
  private void addPlayers(byte n) {
    for(int i = 0; i < n; i++) {
      playersList.add(new Player((byte) (i+1)));
      playersList.get(i).resetCurrentTiles();
    }
  }
  
  private void emptyBoard() {
    for(int r = 0; r < board.length; r++) {
      for(int c = 0; c < board[r].length; c++) {
        board[r][c] = new Tile('?');
        float boardTileX = board[r][c].getOffsetX() + board[r][c].getSize() * r * board[r][c].getSpacing() / 10;
        float boardTileY = board[r][c].getOffsetY() + board[r][c].getSize() * c * board[r][c].getSpacing() / 10;
        board[r][c].setPosition(boardTileX, boardTileY);
        board[r][c].setInitPosition(boardTileX, boardTileY);
      }
    }
  }

  private void addTilesToTilesRemaining() {
    addTile('A', 9);
    addTile('B', 2);
    addTile('C', 2);
    addTile('D', 4);
    addTile('E', 12);
    addTile('F', 2);
    addTile('G', 3);
    addTile('H', 2);
    addTile('I', 9);
    addTile('J', 1);
    addTile('K', 1);
    addTile('L', 4);
    addTile('M', 2);
    addTile('N', 6);
    addTile('O', 8);
    addTile('P', 2);
    addTile('Q', 1);
    addTile('R', 6);
    addTile('S', 4);
    addTile('T', 6);
    addTile('U', 4);
    addTile('V', 2);
    addTile('W', 2);
    addTile('X', 1);
    addTile('Y', 2);
    addTile('Z', 1);
    addTile('*', 2);
  }
  
  /**
   * Add tile n times to tilesRemaining
   */
  private void addTile(char val, int n) {
    for(int i = 0; i < n; i++) {
      tilesRemaining.add(new Tile(val));
    }
  }
  
  /**
   * @return True if the board is empty OR the board tile is available and is adjacent to an occupied board tile
   */
  private boolean validTilePlacement(int r, int c) {
    return boardIsEmpty() || (board[r][c].isEmpty() && adjacentOccupied(r, c));
  }
  
  private boolean boardIsEmpty() {
    for(Tile[] row : board) {
      for(Tile boardTile : row) {
        if(!boardTile.isEmpty()) {
          return false;
        }
      }
    }
    return true;
  }
  
  private boolean adjacentOccupied(int r, int c) {
    return (r > 0 && !board[r-1][c].isEmpty()) // Top occupied
           || (r < board.length - 1 && !board[r+1][c].isEmpty()) // Bottom occupied
           || (c > 0 && !board[r][c-1].isEmpty()) // Left occupied
           || (c < board[r].length - 1 && !board[r][c+1].isEmpty()); // Right occupied
  }
  
  /**
   * Remove from scrabble solver words above max length of current tiles (for efficiency)
   */
  private void wordsRemoveAboveMax() {
    for(int i = 0; i < scrabbleSolver.getWords().size(); i++) {
      if(scrabbleSolver.getWords().get(i).length() > maxLengthCurrentTiles) {
        scrabbleSolver.getWords().remove(i);
      }
    }
  }
  
  public Tile[][] getBoard() {
    return board;
  }

  public Tile getSelectedTile() {
    return selectedTile;
  }

  public Player getCurrentPlayer() {
    return currentPlayer;
  }
  
  public ArrayList<Tile> getTilesRemaining() {
    return tilesRemaining;
  }
  
  public String getBestWord() {
    return bestWord;
  }

  public ArrayList<String> getPossibleWords() {
    return possibleWords;
  }
  
  public ScrabbleSolver getScrabbleSolver() {
    return scrabbleSolver;
  }

  public Button getResetButton() {
    return resetButton;
  }

  public Button getDrawButton() {
    return drawButton;
  }

  public int getWIDTH() {
    return WIDTH;
  }

  public int getHEIGHT() {
    return HEIGHT;
  }

  public boolean isEndGame() {
    return endGame;
  }

  public void setEndGame(boolean endGame) {
    this.endGame = endGame;
  }
  
  public boolean isDisplayingScreenSolver() {
    return displayingScreenSolver;
  }

  public void setDisplayingScreenSolver(boolean enabled) {
    this.displayingScreenSolver = enabled;
  }
}