import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class ScrabbleSolver {
  
  private final ArrayList<String> words;
  
  public ScrabbleSolver() {
    words = new ArrayList<String>();
    Scanner sc;
    try {
      sc = new Scanner(new File("scrabbledictionary.txt"));
      while(sc.hasNext()) {
        words.add(sc.next());
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }
  
  /**
  * Returns an ArrayList<String> of all valid scrabble words.
  * @param currentTiles is a string of the tiles the player may use to make a word
  */
  public ArrayList<String> possibleWords(String currentTiles) {
    char[] tiles = currentTiles.toCharArray();
    ArrayList<String> possible = new ArrayList<String>();
    for(String word : words) {
      for(char letter : tiles) {
        if(word.indexOf(letter) > 0
           && possible.indexOf(word) < 0
           && word.length() <= tiles.length
           && notTooManyLetters(word.toCharArray(), tiles)
           && sameLetters(word.toCharArray(), tiles)) {
          possible.add(word);
        }
      }
    }
    return possible;
  }
  
  /**
  * Returns a String that is the word worth the most scrabble points
  */
  public String wordWorthMostPoints(ArrayList<String> possibleWords) {
    String highestScoreWord = new String();
    int highestScore = 0;
    for(String s : possibleWords) {
      if(getScore(s) > highestScore) {
        highestScore = getScore(s);
        highestScoreWord = s;
      }
    }
    return highestScoreWord;
  }
  
  /**
  * Returns the score of a given word according to the Scrabble scoring
  */
  public int getScore(String word) {
    char[] arr = word.toCharArray();
    int score = 0;
    for(char c : arr) {
      score += score(c);
    }
    return score;
  }
  
  public int score(char value) {
    switch(value) {
      case 'A': return 1;
      case 'B': return 3;
      case 'C': return 3;
      case 'D': return 2;
      case 'E': return 1;
      case 'F': return 4;
      case 'G': return 2;
      case 'H': return 4;
      case 'I': return 1;
      case 'J': return 8;
      case 'K': return 5;
      case 'L': return 1;
      case 'M': return 3;
      case 'N': return 1;
      case 'O': return 1;
      case 'P': return 3;
      case 'Q': return 10;
      case 'R': return 1;
      case 'S': return 1;
      case 'T': return 1;
      case 'U': return 1;
      case 'V': return 4;
      case 'W': return 4;
      case 'X': return 8;
      case 'Y': return 4;
      case 'Z': return 10;
      default: return 0;
    }
  }
  
  private int getNumValue(char[] string, char val) {
    int count = 0;
    for(char c : string) {
      if(c == val) {
        count++;
      }
    }
    return count;
  }
  
  /**
   * Number of letters in word is less than or equal to the number of letters in currentTiles
   */
  private boolean notTooManyLetters(char[] word, char[] currentTiles) {
    for(char letter : currentTiles) {
      if(getNumValue(word, letter) > getNumValue(currentTiles, letter)) {
        return false;
      }
    }
    return true;
  }
  
  private boolean sameLetters(char[] word, char[] currentTiles) {
    for(char letter : word) {
      if(new String(currentTiles).indexOf(letter) < 0) {
        return false;
      }
    }
    return true;
  }
  
  public ArrayList<String> getWords() {
    return words;
  }
  
}